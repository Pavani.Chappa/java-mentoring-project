package PetShop;

abstract  class  Pet {

  abstract String getAnimalName();

  abstract float getAnimalPrice();

  abstract float getCostOfPetFoodPerWeek();

  abstract void animalSound();

  public void calculateEligibility(float totalCost){
  if (totalCost >= (getAnimalPrice() + getCostOfPetFoodPerWeek()))
  System.out.println("You have  got enough money to buy and feed a " + getAnimalName());
  else System.out.println("You have not got enough money to buy and feed a " + getAnimalName());
  }
}




