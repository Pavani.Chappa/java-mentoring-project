package PetShop;

public class Bird extends Pet {
  private final String animalName = "bird";
  private final float animalPrice = 700;
  private final float costOfPetFoodPerWeek = 5;

  public Bird() {
  }

  public String getAnimalName() {
    return animalName;
  }

  public float getAnimalPrice() {
    return animalPrice;
  }

  public float getCostOfPetFoodPerWeek() {
    return costOfPetFoodPerWeek;
  }

  public void animalSound() {
    System.out.println(" your bird chirps and would like to sing for you");
  }
}
