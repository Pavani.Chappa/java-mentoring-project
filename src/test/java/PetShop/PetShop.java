package PetShop;
/*Create a Petshop programme demonstrating the fundamental Java principals of Inheritance and Polymorphism. It should takes a user input of a pet,
an amount of cash to spend and an amount of weekly pocket money, then use the following class fields to determine the output of
whether or not the user can afford their chosen pet:
petType
price
costOfPetFood
timesToFeedADay
It should also tell them their pet's sound and activity.
The output should look something like (e.g for pet type Dog):
"you have enough for a Dog. your Dog says woof and would like to go for a walk”
or
"I'm sorry, you cannot afford to buy and feed your pet each week."
*/

import java.util.Scanner;

public class PetShop {

  public static void main(String[] args) {
    System.out.println("Enter pet type");
    Scanner inputPetType = new Scanner(System.in);
    String str = inputPetType.nextLine();

    System.out.println("Enter amount you can spend");
    Scanner inputCashYouHaveToSpend = new Scanner(System.in);
    float amount1 = inputCashYouHaveToSpend.nextFloat();

    System.out.println("Enter weekly pocket money");
    Scanner inputWeeklyPocketMoney = new Scanner(System.in);
    float amount2 = inputWeeklyPocketMoney.nextFloat();

    float totalAmount = amount1 + amount2;

    petShopLogic(str, totalAmount);
  }

  public static void petShopLogic(String petName, float totalPrice) {
    switch (petName) {
      case "dog":
        Dog dog = new Dog();
        dog.calculateEligibility(totalPrice);
        dog.animalSound();
        break;
      case "cat":
        Cat cat = new Cat();
        cat.calculateEligibility(totalPrice);
        cat.animalSound();
        break;
      case "bird":
        Bird bird = new Bird();
        bird.calculateEligibility(totalPrice);
        bird.animalSound();
        break;
      default:
        System.out.println("The pet you have entered is not available with us. Thanks");
    }
  }
}


