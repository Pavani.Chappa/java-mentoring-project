package PetShop;

public class Dog extends Pet {
  private final String animalName ="dog";
  private final float animalPrice=500;
  private final float costOfPetFoodPerWeek=10;

  public Dog() {
  }

  public String getAnimalName() {
    return animalName;
  }

  public float getAnimalPrice() {
    return animalPrice;
  }

  public float getCostOfPetFoodPerWeek() {
    return costOfPetFoodPerWeek;
  }

  public void animalSound() {
    System.out.println(" your Dog says woof and would like to go for a walk");
  }
}