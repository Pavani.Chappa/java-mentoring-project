package PetShop;

public class Cat extends Pet {
  private final String animalName = "cat";
  private final float animalPrice = 600;
  private final float costOfPetFoodPerWeek = 15;

  public Cat() {
  }

  public String getAnimalName() {
    return animalName;
  }

  public float getAnimalPrice() {
    return animalPrice;
  }

  public float getCostOfPetFoodPerWeek() {
    return costOfPetFoodPerWeek;
  }

  public void animalSound() {
    System.out.println(" your cat says meow and would like to play with you");
  }

}
