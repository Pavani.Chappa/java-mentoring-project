package PiggyBank;

public enum item {
  MOVIE_TICKET(25, "Movie Ticket"),
  VIDEO_GAME(100,"Video Game"),
  BIKE(500,"Bike");
  private final float value;
  private final String description;

  item(float value, String description) {
    this.value = value;
    this.description = description;
  }

  public float getValue() {
    return value;
  }

  public String getDescription() {
    return description;
  }

}
